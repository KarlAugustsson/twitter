<?php
class db{
	private static $_instance=null;
	private $_db,

			$_query,
			$count,
			$_error,
			$_pdo;
	public $_result;		
	public static function getInstance(){
		if(!isset(self::$_instance)){
			self::$_instance=new db();
		}
		return self::$_instance;
	}
	public function __construct(){
		try{
			$this->_pdo=new pdo("mysql:host=".config::get("mysql/host").
				";dbname=".config::get("mysql/db"),
				config::get("mysql/username"),
				config::get("mysql/password"));			
		}catch(PDOException$e){
			die($e->getMessage());
		}

	}
	public function query($sql,array $values){
		$this->_query=$this->_pdo->query($sql);
			$this->_query=$this->_pdo->prepare($sql);
			$x=1;
			foreach($values as $value){
				$this->_query->bindValue($x,$value);
				$x++;
			}			
		

	if($this->_query->execute()){
	$this->_result=$this->_query->fetchAll(PDO::FETCH_OBJ);

	$this->_count=$this->_query->rowCount();
	return $this;		
	}
	$this->_count=0;
	return $this;

	}
	public function getResults(){

		return $this->_result;
	}

		public function getCount(){
		return $this->_count;
		
	}
}