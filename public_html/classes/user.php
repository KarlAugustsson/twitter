<?php
class user{
	private $_result;
	private $_finalResultFlag;
	public function loginUser($username,$password){
		if($this->userExist($username,$password)==true){
				$this->_result=$this->_result->getResults()[0];
				session::start();
				session::set("user",[
				"id"=>$this->_result->id,
				"fullname"=>$this->_result->fullname,
				"nickname"=>$this->_result->nickname,
				"email"=>$this->_result->email,
				"avatar"=>$this->_result->avatar

				]);
				header::redirect("../home.php");
			return true;
		}else{
			//header::flash("index.php",'status',["color"=>"red","message"=>"No user with those credentials"]);
		}
	}
	public function userExist($username,$password){
		$sql="call userLogin(?)";
		$params=[$username->value];
		$this->_result=db::getInstance()->query($sql,$params);
		;
		if($this->_result->getCount()==1&&password_verify($password->value,$this->_result->getResults()[0]->password)===true){

			return true;
		}
		return false;
	}
	private function setUser(){
			session::set("user",[
				"userid"=>$this->_result->id,"name"=>$this->_result->firstname,"lastname"=>$this->_result->lastname,"password"=>$this->_result->password,"email"=>$this->_result->email
				]);	
			return true;
	}
	public function checkEmailExist($email){
		$sql="call getEmail(?)";

			$this->_result=db::getInstance()->query($sql,[$email]);

			if($this->_result->getCount()===0){
				

				return true;
		}
			return false;
		}
		//header::flash("registeruser.php",'status',["color"=>"red","message"=>$validate->getErrors()]);	



		public function checkNicknameExist($nickname){
		$sql="call getNickname(?)";

			$this->_result=db::getInstance()->query($sql,[$nickname]);

			if($this->_result->getCount()===0){
				

				return true;
		}else{
			return false;
		}
		
		}
	public function printReplies($id,$start){
	if($this->_result->getCount()===10):
	// jag är koko men ifall man får in 10 resultat
	// då finns det sanolikt mer resultat att hämta

		?>
		<div class="answer-container">
		<a href="home.php" id="<?php print $id;?>" onclick="return getReplies(this,<?php print $start+10;?>)"> Show more replies</a>

	<?php endif;
		$count=$this->_result->getCount()-1;
		$result=$this->_result->getResults();
			for($x=$count;$x>=0;$x--):?>

			<img src="<?php print $result[$x]->avatar?>" height='50px' width='50px' />
			<h5><?php print $result[$x]->fullname;?></h5>
				<i><?php print $result[$x]->publish_date;?></i>
				<p><?php print $result[$x]->message;?></p>

			



		<?php endfor;?>
	</div>
<?php
}
		public function getwallResults($uri){?>

		<div id='post' >
		<?php 
		$count=$this->_result->getCount();
		$result=$this->_result->getResults();
		for($x=0;$x<$count;$x++):
		?>
			<div class="messagecontainer">
				
				<div class="wall-inner-message">
					
					<img src="<?php print $result[$x]->avatar;?>"/>
						
						<h5><?php print $result[$x]->fullname;?></h5>
							
							<i><?php print $result[$x]->nickname;?></i>

							
							<i><?php print $result[$x]->publish_date;?></i>
				
					<?php if($result[$x]->originalAuthor!=""):?>

							<i>this post has been retweeted from

							 <?php print $result[$x]->originalAuthor;?>

							</i>
		
		<?php endif;?>
			<p><?php print $result[$x]->message;?></p>
		<?php if($result[$x]->attached_img!=""):?>

			<img src="<?php print $result[$x]->attached_img;?>"/>
		<?php endif;?>
		<?php if($result[$x]->replies>=1):?>
			<a href="<?php print $uri ;?>?replymessageid=<?php print $result[$x]->id;?>"
				 id='<?php print $result[$x]->id?>' onclick='return getReplies(this,0)'>
				  Show replies(<?php print $result[$x]->replies;?>)</a>
		<?php endif;?>

		</div>

		<div class="message-options">
		

					<i id="<?php print $result[$x]->id;?>"
						
						<?php if($result[$x]->ilike==1):?>
							
							class="likebutton active"
						
						<?php else:?>

							class="likebutton"

						<?php endif;?>

					onclick="togglelike(this)">
					<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
							
						<?php print $result[$x]->likes;?>

					</i>

					<i id="<?php print $result[$x]->id;?>"

						<?php if($result[$x]->iretweet==1):?>

						class="retweetbutton active"

					<?php else:?>

						class="retweetbutton"

					<?php endif;?>	

					onclick="toggleretweet(this)">
					<span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>

					<?php print $result[$x]->retweets;?>

					</i>


			
					<form action="functions/answertweet.php" method="POST" name="answertweet" id="<?php print $result[$x]->id;?>">

					<textarea name="tweetMessage" cols="30" rows="4" placeholder="Write Something"></textarea>
					<input name="sendbutton" type="submit" value="Send" id="<?php print $result[$x]->id?>" onclick="return submitReply(this.parentNode)">
				</form>
		</div>

				</div>
				

	<?php
	 endfor;?>
	</div>

<?php	
}
		public function getWallResultsnotloggedin($uri){?>

		<div id='post' >
		<?php 
		$count=$this->_result->getCount();
		$result=$this->_result->getResults();
		for($x=0;$x<$count;$x++):
		?>
			<div class="messagecontainer">
				
				<div class="wall-inner-message">
					
					<img src="<?php print $result[$x]->avatar;?>"/>
						
						<h3><?php print $result[$x]->fullname;?></h3>
							
							<i><?php print $result[$x]->nickname;?></i>

							
							<i><?php print $result[$x]->publish_date;?></i>
				
					<?php if($result[$x]->originalAuthor!=""):?>

							<i>this post has been retweeted from

							 <?php print $result[$x]->originalAuthor;?>

							</i>
		
		<?php endif;?>
			<p><?php print $result[$x]->message;?></p>
		<?php if($result[$x]->attached_img!=""):?>

			<img src="<?php print $result[$x]->attached_img;?>"/>
		<?php endif;?>


		</div>

		<div class="message-options">
		

					<i id="<?php print $result[$x]->id;?>"
						
						<?php if($result[$x]->ilike==1):?>
							
							class="likebutton active"
						
						<?php else:?>

							class="likebutton"

						<?php endif;?>

					>like
							
						<?php print $result[$x]->likes;?>
						
						<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
					</i>

					<i id="<?php print $result[$x]->id;?>"

						<?php if($result[$x]->iretweet==1):?>

						class="retweetbutton active"

					<?php else:?>

						class="retweetbutton"

					<?php endif;?>	

					>retweet

					<?php print $result[$x]->retweets;?>

					</i>


			

		</div>

				</div>
				

	<?php
	 endfor;?>
	</div>

<?php	
}
public function updatewall($uri){
		$count=$this->_result->getCount();
		$result=$this->_result->getResults();
		for($x=0;$x<$count;$x++):
		?>
			<div class="messagecontainer">
				
				<div class="wall-inner-message">
					
					<img src="<?php print $result[$x]->avatar;?>"/>
						
						<h3><?php print $result[$x]->fullname;?></h3>
							
							<i><?php print $result[$x]->nickname;?></i>

							
							<i><?php print $result[$x]->publish_date;?></i>
				
					<?php if($result[$x]->originalAuthor!=""):?>

							<i>this post has been retweeted from

							 <?php print $result[$x]->originalAuthor;?>

							</i>
		
		<?php endif;?>
			<p><?php print $result[$x]->message;?></p>
		<?php if($result[$x]->attached_img!=""):?>

			<img src="<?php print $result[$x]->attached_img;?>"/>
		<?php endif;?>
		<?php if($result[$x]->replies>=1):
			if(session::loggedin()==true):?>
			<a href="<?php print $uri ;?>?replymessageid=<?php print $result[$x]->id;?>"
				 id="<?php print $result[$x]->id?>" onclick="return getReplies(this,0)">
				  Show replies(<?php print $result[$x]->replies;?>)</a>
				<?php endif;?>
		<?php endif;?>

		</div>

		<div class="message-options">
		

					<i id="<?php print $result[$x]->id;?>"
						
						<?php if($result[$x]->ilike==1):?>
							
							class="likebutton active"
						
						<?php else:?>

							class="likebutton"

						<?php endif;?>
						<?php if(session::loggedin()==true):?>
					onclick="togglelike(this)">
							<?php endif;?>
						likes<?php print $result[$x]->likes;?>

					</i>

					<i id="<?php print $result[$x]->id;?>"

						<?php if($result[$x]->iretweet==1):?>

						class="retweetbutton active"

					<?php else:?>

						class="retweetbutton"

					<?php endif;?>	
					<?php if(session::loggedin()==true):?>
					onclick="toggleretweet(this)">
					<?php endif;?>
					retweet<?php print $result[$x]->retweets;?>
					<span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
					</i>


			<?php if(session::loggedin()==true):?>
					<form action="functions/answertweet.php" method="POST" name="answertweet" id="<?php print $result[$x]->id;?>">

					<textarea name="tweetMessage" cols="30" rows="4" placeholder="Write Something"></textarea>
					<input name="sendbutton" type="submit" value="Send" id="<?php print $result[$x]->id?>" onclick="return submitReply(this.parentNode)">
				</form>
			<?php endif;?>
		</div>

				</div>
				

	<?php
	 endfor;
	}
public function getUserSearchResult(){?>
<div class="user-search-result-container">
	<div class="inner">

<?php 
$count=$this->_result->getCount();
sanitize::sanitizeThis($this->_result->getResults());
$result=$this->_result->getResults();
for($x=0;$x<$count;$x++):?>
	<div class="result">
		<a href="user.php?user=<?php print $result[$x]->nickname;?>">
		<img src="<?php print $result[$x]->avatar;?>" height='30px' width='30px' />
		<h4><?php print $result[$x]->fullname;?></h4>
		<i><?php print $result[$x]->nickname;?></i>
		</a>
	</div>

	<?php endfor;?>

	</div>

</div>
<?php }
		public function getWall($userid,$uri){
			$sql="call getHomeNews(?,?,?)";
			$params=[$userid,0,10];
			$this->_result=db::getInstance()->query($sql,$params);
			if($this->_result->getCount()>=1){
				sanitize::sanitizeThis($this->_result->getResults());
				$this->getwallResults($uri);
			}
			
			return $this;
		}
	
		public function registerUser($firstname,$lastname,$nickname,$email,$password,$repeat_password,$avatar){
			
			$validate=new validate();
			
			$validate->firstname($firstname);
			
			$validate->lastname($lastname);
			
			$validate->nickname($nickname);

			$validate->email($email);

			$validate->password($password,$repeat_password);
			if($validate->getErrors()===0){

			$sql="call newUser(?,?,?,?,?,?)";
			if($this->_result=db::getInstance()->query($sql,[$firstname->value,$lastname->value,$nickname->value,$email->value,password_hash($password->value,PASSWORD_BCRYPT),$avatar->value])){
				if($this->_result->getCount()===1){
								$this->_result=true;	
					$this->loginUser($nickname,$password);
			
			}

		}

			
			}else{
				
				$this->_result=false;
			}
			
			
				// header::flash("registeruser.php",'status',["color"=>"red","message"=>$validate->getErrors()]);	
		}
		public function newMessage($subject){	
			$sql="call newMessage(?,?,?)";
			if($this->_result=db::getInstance()->query($sql,[$subject->id,$subject->value,""])->getCount()===1){
				return true;
			}else{
				return false;
			}

		}
		public function updatethiswall($id,$rowfloor,$uri){

			$sql="call getHomeNews(?,?,?)";
			$this->_result=db::getInstance()->query($sql,[$id,$rowfloor,10]);
			if($this->_result->getCount()>=1){
				sanitize::sanitizeThis($this->_result->getResults());
				$this->updatewall($uri);
				$this->_finalResultflag=false;
					return $this;
		



			}else{

				$this->_finalResultflag=true;

				return $this;
			}



		}
public function submitMessagereply($author,$receiver,$themessage){
	$sql="call userReplyMessage(?,?,?)";
	$params=[$author,$receiver,$themessage];
	$this->_result=db::getInstance()->query($sql,$params);
	if($this->_result->getCount()===1){
		return true;
	}else{
		return false;
	}
}
public function getMessageReplies($id,$start,$max){
$sql="call getReplies(?,?,?)";
$params=[$id,$start,$max];
$this->_result=db::getInstance()->query($sql,$params);
if($this->_result->getCount()===0){
	return false;
}
sanitize::sanitizeThis($this->_result->getResults());
$this->printReplies($id,$start);
return true;
}
public function toggleLikeMessage($like){
	if($like->toggle=="true"){
		$sql="call userLike(?,?)";
		$params=[$like->liker,$like->themessage];
		$this->_result=db::getInstance()->query($sql,$params);
			if($this->_result->getCount()===1){
				
				return true;
			
			}else{

				return false;
			}
	}else{
		$sql="call userDislikeMessage(?,?)";
		$params=[$like->liker,$like->themessage];
		$this->_result=db::getInstance()->query($sql,$params);
			if($this->_result->getCount()===1){
				
				return true;
			
			}else{

				return false;
			}

	}

}
public function toggleretweetMessage($retweet){
	if($retweet->toggle=="true"){
		$sql="call userRetweet(?,?)";
		$params=[$retweet->retweeter,$retweet->themessage];
		$this->_result=db::getInstance()->query($sql,$params);
			if($this->_result->getCount()===1){
				return true;
			
			}else{

				return false;
			}
	}else{
		$sql="call userRetweetNoLonger(?,?)";
		$params=[$retweet->retweeter,$retweet->themessage];
		$this->_result=db::getInstance()->query($sql,$params);
			if($this->_result->getCount()===1){
				
				return true;
			
			}else{

				return false;
			}

	}

}
public function searchforUsers($string){
	$sql="call searchUser(?)";
	$params=[$string];
	$this->_result=db::getInstance()->query($sql,$params);

		if($this->_result->getCount()>=1){
			sanitize::sanitizeThis($this->_result->getResults());
			$this->getUserSearchResult();
			
			return true;
		
		}else{

			return false;
		}
}
public function getUserWall($loggedinuser,$useraskedfor,$uri){
	$sql="call getUserProfile(?,?,@nickid)";
	$params=[$loggedinuser,$useraskedfor];
	$this->_result=db::getInstance()->query($sql,$params);
		if($this->_result->getCount()>=1){
			sanitize::sanitizeThis($this->_result->getResults());
			$this->getWallResults($uri);
			return true;
		
		}else{

			return false;
		}
}
public function getNotloggedInUserWall($useraskedfor,$uri){
	$sql="call getUserProfile(?,?,@nickid)";
$params=[0,$useraskedfor];
$this->_result=db::getInstance()->query($sql,$params);
	if($this->_result->getCount()>=1){
		sanitize::sanitizeThis($this->_result->getResults());
		$this->getWallResultsnotloggedin($uri);
		return true;
	
	}else{

		return false;
	}
}


public function getuserinfo($user){
		$sql="call getUserInfo(?)";
	$params=[$user];
	$this->_result=db::getInstance()->query($sql,$params);
		if($this->_result->getCount()===1){
			return $this->_result;
		
		}else{

			return false;
		}
}
public function doyouFollow($liker,$persontolike){
	$sql="call doyouFollow(?,?,@nickid)";
	$params=[$liker,$persontolike];
	$this->_result=db::getInstance()->query($sql,$params);
	if($this->_result->getCount()==1){
		if($this->_result->getResults()[0]->follow==1){
			return true;
		
		}else{

			return false;
		}	
	}else{

		return false;
		}
}

public function followUser($liker,$persontolike){
	$sql="call followUser(?,?,@usernickid)";
	$params=[$liker,$persontolike];
	$this->_result=db::getInstance()->query($sql,$params);
		if($this->_result->getCount()==1){
			return true;
		
		}else{

			return false;
		}
}
public function unfollowUser($liker,$persontolike){
	$sql="call followUserNoLonger(?,?,@usernickid)";
	$params=[$liker,$persontolike];
	$this->_result=db::getInstance()->query($sql,$params);
		if($this->_result->getCount()==1){

			return true;
		
		}else{

			return false;
		}
}
		public function getResult(){
		return $this->_result;	
		}
		public function getResultFlag(){
		return $this->_finalResultflag;	
		}
}