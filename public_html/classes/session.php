<?php
class session{
	private static $_sessionflag=false;

	public static function get($key,$secondkey=null){
		if($secondkey==true){
			if(isset($_SESSION[$key][$secondkey]))
				return $_SESSION[$key][$secondkey];
		}else{
			if(isset($_SESSION[$key]))
			return $_SESSION[$key];
		}
		return false;
	}
	public static function set($key,$value){
		$_SESSION[$key]=$value;
	}
	public static function start(){
		if(self::$_sessionflag==false){
			session_start();
			self::$_sessionflag=true;
		}

	}
	public static function display(){
		if(!empty($_SESSION)){
		print "<pre>";
		print_r($_SESSION);
		print"</pre>";		
		}
	}
	public static function printstatus($name,$values){
		if(session::get($name)!=false){
		print "<span style='color:".session::get($name,$values[0]).";'>".session::get($name,$values[1])."</span>";
		session::destroy($name);

		}
		return false;


	}
	public static function loggedIn(){
		if (session::get("user")!=false){

			return true;
		}
			
		return false;

		}
	public static function destroy($key=null){
		if(self::$_sessionflag==true){
			if($key==null){
				session_destroy();
			}else{
				if(isset($_SESSION[$key]))
				unset($_SESSION[$key]);					
			}
		
		}
			return false;
	}
}

