<?php
require_once("init/init.php");
session::start();
if(!isset($_GET["user"])){
	die("no user was asked for page closed");	
}else{
$useraskedfor=$_GET["user"];
$userloggedin=session::get("user","id");
}
if(session::loggedin()==true){
	if($useraskedfor==session::get("user","nickname")){
		header("Location:userProfile.php");
	}
	require("content/head.php");
	require_once("functions/getLoggedInUserProfile.php");
	
}else{
	require_once("functions/getNotLoggedinUserProfile.php");
	
}
require_once("functions/updatewall.php");
?>
<script src="js/updatewall.js"></script>
<script src="js/managereplies.js"></script>
<script src="js/manageretweet.js"></script>
<script src="js/managelikes.js"></script>
<script src="js/togglefollow.js"></script>
<?php require("content/foot.php");?>
