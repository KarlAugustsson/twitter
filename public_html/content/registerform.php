<?php
require_once("init/init.php");
		
		$firstname= new stdclass;
		$firstname->value="";
		$firstname->errormsg="";

		$lastname= new stdclass;
		$lastname->value="";
		$lastname->errormsg="";

		$nickname= new stdclass;
		$nickname->value="";
		$nickname->errormsg="";

		$email= new stdclass;
		$email->value="";
		$email->errormsg="";

		$password= new stdclass;
		$password->value="";
		$password->errormsg="";

		$repeat_password= new stdclass;
		$repeat_password->value="";
		$repeat_password->errormsg="";
	if($_SERVER['REQUEST_METHOD']=="POST"){
		REQUIRE_ONCE("functions/registerUser.php");
	
}
	?><div class="form-container" id="form-container">
	
		<form action="index.php" id="register_form" name="register_form" method="POST">
			<label for="firstname" value="">
				
				<p>Firstname:</p>
				
				<input type="text" id="firstname" name="firstname" value="<?=$firstname->value?>">
				
				<div class="validate-box" id="firstname-validate" value=""><?=$firstname->errormsg?></div>

			</label>
			
			<label for="lastname" value="lastname" id="">
				
				<p>Lastname:</p>

				<input type="text" id="lastname" name="lastname" value="<?=$lastname->value?>">


				<div class="validate-box" id="lastname-validate" value=""><?=$lastname->errormsg?></div>
			
			</label>

			<label for="nickname" value="Nickname:">
				
				<p>Nickname:</p>

				<input type="text" id="nickname" name="nickname" value="<?=$nickname->value?>">
			
				<div class="validate-box" id="nickname-validate" value=""><?=$nickname->errormsg?></div>


			</label>

			<label for="email" value="Email:">
				
			<p>Email:</p>

			<input type="text" id="email" name="email" value="<?=$email->value?>">
			
			<div class="validate-box" id="email-validate" value=""><?=$email->errormsg?></div>

			</label>

			<label for="password" value="Password">
				
			<p>Password :</p>

			<input type="text" id="password" name="password" value="<?=$password->value?>">

			<div class="validate-box" id="password-validate" value=""><?=$password->errormsg?></div>

			</label>

			<label for="repeat-password" value="Repeat Password">
				
				<p>Repeat Password:</p>

				<input type="text" id="repeat-password" name="repeat-password" value="<?=$repeat_password->value?>">

				<div class="validate-box" id="repeat-password-validate" value=""><?=$repeat_password->errormsg?></div>

			</label>
			<br>
			<br>
			<input type="submit" value="register" id="submit" name="submit">

			<div class="validate-box" id="submit-validate"></div>
		
		</form>
	
	</div>

	<script src="js/registeruser.js"></script>
