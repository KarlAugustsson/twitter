<?php
require_once("init/init.php");
if($_POST){
	$firstname= new stdclass;
	$firstname->name="firstname";
	$firstname->value=strtolower($_POST["firstname"]);
	$firstname->minvalue=1;
	$firstname->maxvalue=50;
	$firstname->pattern="[^a-zA-Z -]";
	$firstname->status="false";
	$firstname->errormsg="";

	$lastname= new stdclass;
	$lastname->name="lastname";
	$lastname->value=strtolower($_POST["lastname"]);
	$lastname->minvalue=1;
	$lastname->maxvalue=50;
	$lastname->pattern="[^a-zA-Z -]";
	$lastname->status="false";
	$lastname->errormsg="";

	$nickname= new stdclass;
	$nickname->name="nickname";
	$nickname->value=strtolower($_POST["nickname"]);
	$nickname->minvalue=1;
	$nickname->maxvalue=50;
	$nickname->pattern="[^a-zA-Z0-9.\'_'-]";
	$nickname->status="false";
	$nickname->errormsg="";

	$email= new stdclass;
	$email->name="email";
	$email->value=strtolower($_POST['email']);
	$email->minvalue=3;
	$email->maxvalue=100;
	$email->status="false";
	$email->errormsg="";

	$password= new stdclass;
	$password->name="password";
	$password->value=$_POST["password"];
	$password->minvalue=5;
	$password->maxvalue=30;
	$password->status="false";
	$password->errormsg="";

	$repeat_password= new stdclass;
	$repeat_password->name="repeat-password";
	$repeat_password->value=$_POST["repeat-password"];
	$repeat_password->status="false";
	$repeat_password->errormsg="";
	$avatar=new stdclass;
	$avatar->value="http://www.gravatar.com/avatar/".md5(trim($email->value))."?d=mm";

	$register= new user();
	$register->registerUser($firstname,$lastname,$nickname,$email,$password,$repeat_password,$avatar);

		}else{
			header("location:index.php?error");
		}
