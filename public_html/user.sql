-- MySQL dump 10.13  Distrib 5.6.19, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: kaau
-- ------------------------------------------------------
-- Server version	5.6.19-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `show_messages`
--

DROP TABLE IF EXISTS `show_messages`;
/*!50001 DROP VIEW IF EXISTS `show_messages`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `show_messages` AS SELECT 
 1 AS `message`,
 1 AS `replies`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `test`
--

DROP TABLE IF EXISTS `test`;
/*!50001 DROP VIEW IF EXISTS `test`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `test` AS SELECT 
 1 AS `desperate_cow_id`,
 1 AS `message_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `test2`
--

DROP TABLE IF EXISTS `test2`;
/*!50001 DROP VIEW IF EXISTS `test2`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `test2` AS SELECT 
 1 AS `message_id`,
 1 AS `copycat_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `testview`
--

DROP TABLE IF EXISTS `testview`;
/*!50001 DROP VIEW IF EXISTS `testview`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `testview` AS SELECT 
 1 AS `name`,
 1 AS `message`,
 1 AS `publish_date`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nickname` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `avatar` varchar(100) NOT NULL,
  `account_status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (65,'karlaug','karl','augustsson','karl.augustsson@gmail.com','$2y$10$8VpQpdijKT.Je3K/NhFQ8esp/etq.hb4szOjqmjyoa6zsT0s0dow6','http://www.gravatar.com/avatar/d2667744b06d42f9ecb628cba480e220?d=mm',0,'2015-02-28 14:33:32'),(66,'elinaug','elin','augustsson','elin.augustsson@gmail.com','$2y$10$buINW3UlJpL5Nzu0A2lKxOJ7JL9ssE0O51BO0rRyLs9dgGezUPPj.','http://www.gravatar.com/avatar/6482ec3fdb69a9cd8c8d65b540c3c11a?d=mm',0,'2015-02-28 14:36:17');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_follower`
--

DROP TABLE IF EXISTS `user_follower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_follower` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persontolike_id` int(10) unsigned NOT NULL,
  `theonewholikes_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `persontolike_id` (`persontolike_id`),
  KEY `theonewholikes_id` (`theonewholikes_id`),
  CONSTRAINT `user_follower_ibfk_1` FOREIGN KEY (`persontolike_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_follower_ibfk_2` FOREIGN KEY (`theonewholikes_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_follower`
--

LOCK TABLES `user_follower` WRITE;
/*!40000 ALTER TABLE `user_follower` DISABLE KEYS */;
INSERT INTO `user_follower` VALUES (19,65,66),(80,66,65);
/*!40000 ALTER TABLE `user_follower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `user_likes`
--

DROP TABLE IF EXISTS `user_likes`;
/*!50001 DROP VIEW IF EXISTS `user_likes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `user_likes` AS SELECT 
 1 AS `liker_id`,
 1 AS `liker`,
 1 AS `persontolike_id`,
 1 AS `likes`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user_messages`
--

DROP TABLE IF EXISTS `user_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message` varchar(500) NOT NULL,
  `include_img` varchar(100) DEFAULT NULL,
  `publish_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_messages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_messages`
--

LOCK TABLES `user_messages` WRITE;
/*!40000 ALTER TABLE `user_messages` DISABLE KEYS */;
INSERT INTO `user_messages` VALUES (36,'trevligt','','2015-02-28 14:36:22',66),(37,'hett','','2015-02-28 14:53:51',66),(38,'tack gode gud','','2015-02-28 17:09:24',65),(39,'teeeeeee','','2015-02-28 21:14:46',65);
/*!40000 ALTER TABLE `user_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_messages_favorite`
--

DROP TABLE IF EXISTS `user_messages_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_messages_favorite` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL,
  `desperate_cow_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `message_id` (`message_id`),
  KEY `desperate_cow_id` (`desperate_cow_id`),
  CONSTRAINT `user_messages_favorite_ibfk_1` FOREIGN KEY (`message_id`) REFERENCES `user_messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_messages_favorite_ibfk_2` FOREIGN KEY (`desperate_cow_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_messages_favorite`
--

LOCK TABLES `user_messages_favorite` WRITE;
/*!40000 ALTER TABLE `user_messages_favorite` DISABLE KEYS */;
INSERT INTO `user_messages_favorite` VALUES (69,38,66),(75,39,65),(76,36,65),(77,37,65),(78,37,65);
/*!40000 ALTER TABLE `user_messages_favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_messages_replies`
--

DROP TABLE IF EXISTS `user_messages_replies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_messages_replies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message` varchar(500) NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `message_id` int(10) unsigned NOT NULL,
  `publish_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  KEY `message_id` (`message_id`),
  CONSTRAINT `user_messages_replies_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_messages_replies_ibfk_2` FOREIGN KEY (`message_id`) REFERENCES `user_messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_messages_replies`
--

LOCK TABLES `user_messages_replies` WRITE;
/*!40000 ALTER TABLE `user_messages_replies` DISABLE KEYS */;
INSERT INTO `user_messages_replies` VALUES (9,'trevligt',66,37,'2015-02-28 14:53:55'),(10,'fattar inte vad allt tog vagen',65,37,'2015-02-28 14:54:35'),(11,'men har du verkligen t\'nkt igenom all nu',65,37,'2015-02-28 20:51:03'),(12,'eller \'r du dum i huvet',65,37,'2015-02-28 20:51:14'),(13,'n\'r jag va liten brukade vi prata om livet och snackade inte en massa skit',65,37,'2015-02-28 20:51:57'),(14,'men du kanske gillar att skada andra mÃ¤nniskor',65,37,'2015-02-28 20:52:18'),(15,'nej det var nog allt',65,37,'2015-02-28 20:52:40'),(16,'du fattar att detta Ã¤r allvar va',66,37,'2015-02-28 20:53:04'),(17,'eller ger du upp nu',66,37,'2015-02-28 20:53:36'),(18,'vad fan Ã¤r ditt problem',66,37,'2015-02-28 20:53:47'),(19,'kan du vara lite mer konkret',66,37,'2015-02-28 20:54:20'),(20,'kanske',66,37,'2015-02-28 20:54:47');
/*!40000 ALTER TABLE `user_messages_replies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_messages_retweet`
--

DROP TABLE IF EXISTS `user_messages_retweet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_messages_retweet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL,
  `copycat_id` int(10) unsigned NOT NULL,
  `publish_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `message_id` (`message_id`),
  KEY `copycat_id` (`copycat_id`),
  CONSTRAINT `user_messages_retweet_ibfk_1` FOREIGN KEY (`message_id`) REFERENCES `user_messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_messages_retweet_ibfk_2` FOREIGN KEY (`copycat_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_messages_retweet`
--

LOCK TABLES `user_messages_retweet` WRITE;
/*!40000 ALTER TABLE `user_messages_retweet` DISABLE KEYS */;
INSERT INTO `user_messages_retweet` VALUES (18,37,65,'2015-02-28 19:50:55');
/*!40000 ALTER TABLE `user_messages_retweet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'kaau'
--
/*!50003 DROP PROCEDURE IF EXISTS `doYouFollow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `doYouFollow`(in liker int,in likernick varchar(50),out nickid int)
begin select id into nickid from user where nickname=likernick; select if(theonewholikes_id=liker and persontolike_id=nickid,1,0) as follow from user_follower where theonewholikes_id=liker and persontolike_id=nickid; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `FollowUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FollowUser`(in vid int,in vnickname varchar(50),out usernickid int)
begin select id into usernickid from user where nickname=vnickname; insert into user_follower (theonewholikes_id,persontolike_id) values(vid,usernickid); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `followUserNoLonger` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `followUserNoLonger`(in liker int,in usernick varchar(50),out usernickid int)
begin
select id into usernickid from user where nickname=usernick;
delete from user_follower where persontolike_id=usernickid and theonewholikes_id=liker;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getEmail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getEmail`(in var varchar(50))
begin
select * from user where email=var;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getHomeNews` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getHomeNews`(in vid int,in vstart int,in vmax int)
begin
select um.id,concat(left(upper(u.name),1),substring(u.name,2)," ",left(upper(u.lastname),1),substring(u.lastname,2)) as fullname,
u.nickname,
u.avatar,
um.message,
count(distinct umf.id) as likes,
um.include_img as attached_img,
if((select desperate_cow_id from test where desperate_cow_id=vid and message_id=um.id),true,false)as ilike,
if(originaluser.id=um.user_id,null,originaluser.name) as originalAuthor,
count(distinct umretweet.id) as retweets,
if((select message_id from test2 where test2.copycat_id=vid and test2.message_id=um.id),true,false) as iretweet,
count(distinct umreply.id) as replies,
um.publish_date

from user as u 
left outer join user_messages as um on u.id=um.user_id 
left outer join user_messages_favorite as umf on um.id=umf.message_id 
left outer join user as originaluser on originaluser.id=um.user_id
left outer join user_messages_retweet as umretweet on um.id=umretweet.message_id
left outer join user_messages_replies as umreply on um.id=umreply.message_id
where u.id=vid group by publish_date union





select umretweet.message_id,concat(left(upper(u.name),1),substring(u.name,2)," ",left(upper(u.lastname),1),substring(u.lastname,2)) as fullname,
u.nickname,
u.avatar,
um.message,count(distinct umf.id) as likes,
um.include_img as attached_img,
if((select desperate_cow_id from test where desperate_cow_id=vid and message_id=um.id limit 1),true,false)as ilike,
if(originaluser.id=umretweet.copycat_id,null,originaluser.name) as originalAuthor,
count(distinct umretweet.id) as retweets,
if((select message_id from test2 where test2.copycat_id=vid and test2.message_id=um.id),true,false) as iretweet,
count(distinct umreply.id) as replies,
umretweet.publish_date

from user as u 
inner join user_messages_retweet as umretweet on umretweet.copycat_id=u.id 
left outer join user_messages as um on umretweet.message_id=um.id
left outer join user as originaluser on originaluser.id=um.user_id
left outer join user_messages_replies as umreply on um.id=umreply.message_id
left outer join user_messages_favorite as umf on um.id=umf.message_id 
where u.id=vid group by publish_date union


select um.id,concat(left(upper(u.name),1),substring(u.name,2)," ",left(upper(u.lastname),1),substring(u.lastname,2)) as fullname,
u.nickname,
u.avatar,
um.message,count(distinct umf.id) as likes,
um.include_img as attached_img,
if((select desperate_cow_id from test where desperate_cow_id=vid and message_id=um.id limit 1),true,false)as ilike,
if(originaluser.id=um.user_id,null,originaluser.name) as originalAuthor,
count(distinct umretweet.id) as retweets,
if((select message_id from test2 where test2.copycat_id=vid and test2.message_id=um.id limit 1),true,false) as iretweet,
count(distinct umreply.id) as replies,
um.publish_date

from user as u
inner join user_follower as uf on u.id=persontolike_id
left outer join user_messages as um on u.id=um.user_id 
left outer join user_messages_favorite as umf on um.id=umf.message_id 
left outer join user as originaluser on originaluser.id=um.user_id
left outer join user_messages_retweet as umretweet on um.id=umretweet.message_id
left outer join user_messages_replies as umreply on um.id=umreply.message_id
where uf.theonewholikes_id=vid group by publish_date union


select umretweet.message_id,concat(left(upper(u.name),1),substring(u.name,2)," ",left(upper(u.lastname),1),substring(u.lastname,2)) as fullname,
u.nickname,
u.avatar,
um.message,count(distinct umf.id) as likes,
um.include_img as attached_img,
if((select desperate_cow_id from test where desperate_cow_id=vid and message_id=um.id limit 1),true,false)as ilike,
if(originaluser.id=umretweet.copycat_id,null,originaluser.name) as originalAuthor,
count(distinct umretweet.id) as retweets,
if((select message_id from test2 where test2.copycat_id=vid and test2.message_id=um.id),true,false) as iretweet,
count(distinct umreply.id) as replies,
umretweet.publish_date

from user as u
inner join user_follower as uf on u.id=persontolike_id
inner join user_messages_retweet as umretweet on umretweet.copycat_id=u.id 
left outer join user_messages as um on umretweet.message_id=um.id
left outer join user as originaluser on originaluser.id=um.user_id
left outer join user_messages_replies as umreply on um.id=umreply.message_id
left outer join user_messages_favorite as umf on um.id=umf.message_id 
where uf.theonewholikes_id=vid group by publish_date order by publish_date desc limit vstart,vmax;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getNickname` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getNickname`(in var varchar(50))
begin
select nickname from user where nickname=var limit 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getReplies` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getReplies`(in vid int,in vstart int,in vmax int)
begin select concat(left(upper(u.name),1),substring(u.name,2)," ",left(upper(u.lastname),1),substring(u.lastname,2)) as fullname,u.avatar,um.message,um.publish_date,um.message_id from user as u inner join user_messages_replies as um on u.id=um.author_id where um.message_id=vid order by publish_date desc limit vstart,vmax; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getUserInfo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserInfo`(in vnickname varchar(50))
begin select id,nickname,concat(left(upper(name),1),substring(name,2)," ",left(upper(lastname),1),substring(lastname,2)) as fullname,avatar,register_date from user where nickname=vnickname; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GetUserProfile` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetUserProfile`(in vid int,in vnickname varchar(50),out nickid int)
begin
select id into nickid from user where nickname=vnickname;
select um.id,concat(left(upper(u.name),1),substring(u.name,2)," ",left(upper(u.lastname),1),substring(u.lastname,2)) as fullname,
u.nickname,
u.avatar,
um.message,
count(distinct umf.id) as likes,
um.include_img as attached_img,
if((select desperate_cow_id from test where desperate_cow_id=vid and message_id=um.id limit 1),true,false)as ilike,
if(originaluser.id=um.user_id,null,originaluser.name) as originalAuthor,
count(distinct umretweet.id) as retweets,
if((select message_id from test2 where test2.copycat_id=vid and test2.message_id=um.id limit 1),true,false) as iretweet,
count(distinct umreply.id) as replies,
um.publish_date

from user as u 
left outer join user_messages as um on u.id=um.user_id 
left outer join user_messages_favorite as umf on um.id=umf.message_id 
left outer join user as originaluser on originaluser.id=um.user_id
left outer join user_messages_retweet as umretweet on um.id=umretweet.message_id
left outer join user_messages_replies as umreply on um.id=umreply.message_id
where u.id=nickid group by publish_date union





select umretweet.message_id,concat(left(upper(u.name),1),substring(u.name,2)," ",left(upper(u.lastname),1),substring(u.lastname,2)) as fullname,
u.nickname,
u.avatar,
um.message,count(distinct umf.id) as likes,
um.include_img as attached_img,
if((select desperate_cow_id from test where desperate_cow_id=vid and message_id=um.id limit 1),true,false)as ilike,
if(originaluser.id=umretweet.copycat_id,null,originaluser.name) as originalAuthor,
count(distinct umretweet.id) as retweets,
if((select message_id from test2 where test2.copycat_id=vid and test2.message_id=um.id limit 1),true,false) as iretweet,
count(distinct umreply.id) as replies,
umretweet.publish_date

from user as u 
inner join user_messages_retweet as umretweet on umretweet.copycat_id=u.id 
left outer join user_messages as um on umretweet.message_id=um.id
left outer join user as originaluser on originaluser.id=um.user_id
left outer join user_messages_replies as umreply on um.id=umreply.message_id
left outer join user_messages_favorite as umf on um.id=umf.message_id 
where u.id=nickid group by publish_date order by publish_date desc;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `newMessage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `newMessage`(in vid int,in vmessage varchar(500),in vimg varchar(100))
begin insert into user_messages(user_id,message,include_img)values(vid,vmessage,vimg);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `newUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `newUser`(in cname varchar(50),in clastname varchar(50),in cnickname varchar(50),in cemail varchar(100),in cpassword varchar(100),in cavatar varchar(100))
begin insert into user (name,lastname,nickname,email,password,avatar) values(cname,clastname,cnickname,cemail,cpassword,cavatar); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `searchUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `searchUser`(in _vsearch varchar(200))
begin select avatar,concat(left(upper(u.name),1),substring(u.name,2)," ",left(upper(u.lastname),1),substring(u.lastname,2)) as fullname,nickname from user as u where concat(left(upper(name),1),substring(name,2)," ",left(upper(lastname),1),substring(u.lastname,2)) like CONCAT('%',_vsearch,'%') or nickname like CONCAT('%',_vsearch,'%') limit 10; end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userDislikeMessage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `userDislikeMessage`(in vid int,in vmid int)
begin
delete from user_messages_favorite where message_id=vmid and desperate_cow_id =vid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userkaaulogin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `userkaaulogin`(in vname varchar(100))
begin select id,concat(left(upper(name),1),substring(name,2)," ",left(upper(lastname),1),substring(lastname,2)) as fullname,nickname,email,password,avatar from user where nickname=vname or email=vname;end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userLike` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `userLike`(in vid int,in vmess int)
begin insert into user_messages_favorite(message_id,desperate_cow_id)values(vmess,vid); end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userlogin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `userlogin`(in vname varchar(100))
begin select id,concat(left(upper(name),1),substring(name,2)," ",left(upper(lastname),1),substring(lastname,2)) as fullname,nickname,email,password,avatar from user where nickname=vname or email=vname;end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userReplyMessage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `userReplyMessage`(in vid int,in mid int,m varchar(500))
begin insert into user_messages_replies (author_id,message_id,message)values(vid,mid,m);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userRetweet` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `userRetweet`(in vid int,in mid int)
begin
insert into user_messages_retweet(copycat_id,message_id) values(vid,mid);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userRetweetNoLonger` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `userRetweetNoLonger`(in vid int,in vmid int)
begin
delete from user_messages_retweet where copycat_id=vid and message_id=vmid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `show_messages`
--

/*!50001 DROP VIEW IF EXISTS `show_messages`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `show_messages` AS select `r`.`message` AS `message`,count(`m`.`message`) AS `replies` from (`user_messages` `r` left join `user_messages_replies` `m` on((`r`.`id` = `m`.`message_id`))) group by `m`.`message_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `test`
--

/*!50001 DROP VIEW IF EXISTS `test`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `test` AS select `user_messages_favorite`.`desperate_cow_id` AS `desperate_cow_id`,`user_messages_favorite`.`message_id` AS `message_id` from `user_messages_favorite` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `test2`
--

/*!50001 DROP VIEW IF EXISTS `test2`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `test2` AS select `user_messages_retweet`.`message_id` AS `message_id`,`user_messages_retweet`.`copycat_id` AS `copycat_id` from `user_messages_retweet` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `testview`
--

/*!50001 DROP VIEW IF EXISTS `testview`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `testview` AS select `user`.`name` AS `name`,`user_messages`.`message` AS `message`,`user_messages`.`publish_date` AS `publish_date` from ((`user` join `user_messages` on((`user_messages`.`user_id` = `user`.`id`))) join `user_follower` `f` on((`user`.`id` = `f`.`theonewholikes_id`))) where (`f`.`theonewholikes_id` = 54) union select `user`.`name` AS `name`,`user_messages`.`message` AS `message`,`user_messages`.`publish_date` AS `publish_date` from ((`user` join `user_messages` on((`user_messages`.`user_id` = `user`.`id`))) join `user_follower` `f` on((`user`.`id` = `f`.`persontolike_id`))) where (`f`.`theonewholikes_id` = 54) order by `publish_date` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_likes`
--

/*!50001 DROP VIEW IF EXISTS `user_likes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `user_likes` AS select `user_follower`.`theonewholikes_id` AS `liker_id`,`user`.`name` AS `liker`,`user_follower`.`persontolike_id` AS `persontolike_id`,`u`.`name` AS `likes` from ((`user` join `user_follower` on((`user`.`id` = `user_follower`.`theonewholikes_id`))) join `user` `u` on((`user_follower`.`persontolike_id` = `u`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-03 18:44:07
