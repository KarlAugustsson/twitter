<?php
require("content/head.php");
require_once("functions/mainnavigation.php");
?>
<div class="userprofile">
	<div class="info">
<?php

require_once("init/init.php");
session::start();
if(session::loggedin()==true){
$uri=explode("?",$_SERVER['REQUEST_URI']);

	if(isset($uri[1])){
		$uri=$uri[0]."?".$uri[1];

}else{
	$uri=$uri[0];
}

$getProfile=new user();
$getProfile=$getProfile->getuserinfo(session::get("user","nickname"));
?>
<img src="<?php print $getProfile->_result[0]->avatar?>" alt="">
<h2><?php print $getProfile->_result[0]->fullname?></h2>
<i><?php print $getProfile->_result[0]->nickname?></i>
<i><?php print $getProfile->_result[0]->register_date?></i>
<?php require_once("content/tweetform.php");?>
</div>
<?php
$getwall=new user();?>
<div id="wall">
<?php	
$getwall->getUserWall(session::get("user","id"),session::get("user","nickname"),$uri);
require_once("functions/updatewall.php");	
}else{
	header("location:index.php");
}

?>
</div>
<script src="js/newMessage.js"></script>
<script src="js/updatewall.js"></script>
<script src="js/managereplies.js"></script>
<script src="js/manageretweet.js"></script>
<script src="js/managelikes.js"></script>
<script src="js/togglefollow.js"></script>
<?php require("content/foot.php");?>
